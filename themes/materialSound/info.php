<?php
// Theme Name
$name = 'Material';

// Theme Author
$author = 'Jack Lenone';

// Theme URL
$url = 'https://www.facemesh.cz';

// Theme Version
$version = '1.2.4';

?>