jQuery(document).ready(function($) {
    $('.owl-carousel').owlCarousel({
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    items:6,
    autoplay:true,
    autoplayTimeout:2000,
    stagePadding: 50,
    margin:10,
    dots: true,
    autoplayHoverPause:true,
    responsiveClass: true,
    responsive: {
        // breakpoint from 0 up
        0 : {
            items : 2,
        },
        // breakpoint from 480 up
        480 : {
            items : 2,
        },
        // breakpoint from 768 up
        768 : {
            items : 4,
        },
        1100 : {
            items : 6,
        }
    }
    });
});