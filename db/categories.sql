-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:20 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(35, 'Afro Beat'),
(36, 'Afro Pop'),
(37, 'Traditional Country'),
(38, 'Contemporary'),
(40, 'Pop/Rock'),
(43, 'R and B/Soul'),
(44, 'Gospel'),
(45, 'Jazz'),
(46, 'Reggae'),
(47, 'Ragga'),
(48, 'Dance Hall'),
(49, 'Rap '),
(50, 'Hip Hop'),
(51, 'Zouk'),
(52, 'Folk Music'),
(54, 'Band'),
(56, 'Blues'),
(57, 'Traditional Music'),
(59, 'Urban'),
(60, 'Country'),
(61, 'TNS'),
(62, 'Luga Flow'),
(63, 'House'),
(64, 'Electro'),
(65, 'Afro Soul '),
(66, 'Rhumba'),
(67, 'Bongo Fleva'),
(68, 'Benga'),
(69, 'Taraab'),
(70, 'Spoken Word'),
(71, 'Afro Fusion'),
(72, 'EDM Electronic Dance Music');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
