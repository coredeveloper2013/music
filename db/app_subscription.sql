-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:19 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_subscription`
--

CREATE TABLE `app_subscription` (
  `id` int(11) NOT NULL,
  `subscription_name` varchar(255) NOT NULL,
  `subscription_duration` varchar(255) NOT NULL,
  `subscription_price` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `isActive` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  `isDeleted` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_subscription`
--

INSERT INTO `app_subscription` (`id`, `subscription_name`, `subscription_duration`, `subscription_price`, `product_id`, `isActive`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'silver', 'monthly', '150', 'test pro 123', 'Active', 'Yes', '2017-10-16 09:45:04', '2017-11-17 13:55:00'),
(2, 'premium', '3 months', '1500', '152633', 'Active', 'Yes', '2017-10-16 09:47:31', '2017-11-17 13:54:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_subscription`
--
ALTER TABLE `app_subscription`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_subscription`
--
ALTER TABLE `app_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
