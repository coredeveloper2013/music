-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:18 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_playlist_categories`
--

CREATE TABLE `app_playlist_categories` (
  `id` int(11) NOT NULL,
  `sr_no` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `isActive` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  `isDeleted` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_playlist_categories`
--

INSERT INTO `app_playlist_categories` (`id`, `sr_no`, `category`, `isActive`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 4, 'PLAYLISTS', 'Active', 'No', '2017-12-20 06:52:36', '2018-11-02 13:24:47'),
(2, 7, 'MOOD PLAYLISTS', 'Active', 'No', '2017-12-20 07:54:50', '2018-11-02 13:25:12'),
(3, 5, 'Christmas Time', 'InActive', 'No', '2017-12-22 09:02:20', '2018-03-27 13:59:58'),
(4, 1, 'New Year', 'InActive', 'No', '2017-12-29 08:55:29', '2018-03-14 17:55:18'),
(5, 3, 'Valentine\'s Day', 'InActive', 'No', '2018-02-07 11:46:10', '2018-03-14 17:55:47'),
(6, 2, 'Workout', 'InActive', 'No', '2018-02-27 14:21:33', '2018-03-14 17:55:40'),
(7, 6, 'Relax', 'Active', 'Yes', '2018-02-27 14:22:26', '2018-03-14 17:56:06'),
(8, 6, 'Women\'s Day', 'Active', 'Yes', '2018-03-08 15:34:32', '2018-03-14 17:56:06'),
(9, 6, 'THE BEST OF', 'InActive', 'No', '2018-03-14 14:40:46', '2018-11-02 13:40:10'),
(10, 8, 'Good Friday', 'InActive', 'Yes', '2018-03-30 12:13:41', '2018-03-30 12:15:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_playlist_categories`
--
ALTER TABLE `app_playlist_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_playlist_categories`
--
ALTER TABLE `app_playlist_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
