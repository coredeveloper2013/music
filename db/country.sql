-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:21 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sr_no` int(11) NOT NULL,
  `is_active` enum('true','false') NOT NULL DEFAULT 'true',
  `is_deleted` enum('true','false') NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `sr_no`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Uganda', 2, 'true', 'false', '2018-04-19 08:51:31', '2018-04-19 15:43:38'),
(2, 'Rwanda', 3, 'true', 'false', '2018-04-19 08:51:31', '2018-04-19 21:35:38'),
(3, 'India', 4, 'false', 'false', '2018-04-19 09:44:17', '2018-04-20 11:17:51'),
(4, 'Tanzania', 1, 'true', 'false', '2018-04-19 09:44:17', '2018-04-19 15:12:43'),
(16, 'Sudan', 15, 'false', 'false', '2018-04-19 22:05:52', '2018-04-20 11:18:04'),
(6, 'Lebanon', 6, 'false', 'false', '2018-04-19 14:27:00', '2018-04-20 11:17:53'),
(8, 'Gambia', 7, 'false', 'false', '2018-04-19 21:34:30', '2018-04-20 11:17:54'),
(17, 'Cameroon', 16, 'false', 'false', '2018-04-19 22:05:58', '2018-04-20 11:18:05'),
(10, 'Australia', 9, 'false', 'false', '2018-04-19 21:35:02', '2018-04-20 11:17:55'),
(11, 'Belgium', 10, 'false', 'false', '2018-04-19 21:35:10', '2018-04-20 11:17:56'),
(12, 'Senegal', 11, 'false', 'false', '2018-04-19 21:35:17', '2018-04-20 11:18:00'),
(13, 'Burundi', 12, 'false', 'false', '2018-04-19 21:35:46', '2018-04-20 11:18:01'),
(14, 'Kenya', 13, 'true', 'false', '2018-04-19 21:35:56', '2018-07-03 15:39:30'),
(15, 'zambia', 14, 'false', 'false', '2018-04-19 21:36:03', '2018-04-20 11:18:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
