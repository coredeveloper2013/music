<?php
function PageMain() {
	global $TMPL, $LNG, $CONF, $db, $loggedIn, $settings;
	$feed = new feed();
	$feed->db = $db;
	$feed->url = $CONF['url'];
	$arrData = array();
	if(isset($_SESSION['username']) && isset($_SESSION['password']) || isset($_COOKIE['username']) && isset($_COOKIE['password'])) {	
		$verify = $loggedIn->verify();
		
		if($verify['username']) {
			$feed->user = $verify;
			$feed->username = $verify['username'];
			$feed->id = $verify['idu'];
			$arrData['user_id'] = $verify['idu'];
		}
	}

	$feed->per_page = $settings['sperpage'];
	$feed->categories = $feed->getCategories();
	$feed->time = $settings['time'];
	$feed->l_per_post = $settings['lperpost'];
	
	$TMPL_old = $TMPL; $TMPL = array();
	$skin = new skin('shared/rows'); $rows = '';
	
	// If the $_GET keyword is empty [user]
	if($_GET['q'] == '') {
		header("Location: ".$CONF['url']."/index.php?a=welcome");
	}
	$strSearch = $_GET['q'];
 	$arrData['term'] = $_GET['q'];
 	$arrData['is_live'] = "false";
 	$arrData['is_found'] = "false";
	if($_GET['filter'] == 'artists') {
		/*if($_SERVER["REMOTE_ADDR"]=='103.240.169.187'){*/
			$TMPL['messages'] = $feed->getSearch2(0, $settings['sperpage'], $_GET['q'], $_GET['filter']);	
		/*}else{
			list($tracks, $error) = $feed->searchTracks(0, $_GET['q']);
			$TMPL['messages'] = $tracks;
		}*/
		/*$TMPL['messages'] = $tracks;*/
		if($TMPL['messages'] != '<div class="message-inner"></div>'){
			$arrData['is_found'] = "true";
		}
		$arrData['tab'] = "artist";
	}elseif($_GET['filter'] == 'tracks') {
		list($tracks, $error) = $feed->searchTracks(0, $_GET['q']);
		/*if($_SERVER["REMOTE_ADDR"]=='103.241.45.70'){
			echo "<pre>";
			print_r($tracks);
			echo "</pre>";
			die;
		}*/
		if($tracks != '<div class="message-inner">No results available. Try another search.</div>'){
			$arrData['is_found'] = "true";
		}
		$TMPL['messages'] = $tracks;
		$arrData['tab'] = "track";
	}elseif($_GET['filter'] == 'top') {
		/*if($_SERVER["REMOTE_ADDR"]=='103.240.169.187'){*/
			$TMPL['messages'] = $feed->getSearch2(0, $settings['sperpage'], $_GET['q'], $_GET['filter']);
			/*if($_SERVER["REMOTE_ADDR"]=='103.240.169.187'){*/
			list($tracks, $error) = $feed->searchTop(0, $_GET['q']);
			/*}else{
				list($tracks, $error) = $feed->searchTracks(0, $_GET['q']);
			}*/
			$TMPL['messages'] .= $tracks;
			if($tracks != '<div class="message-inner">No results available. Try another search.</div>'){
				$arrData['is_found'] = "true";
			}
		/*}else{
			list($tracks, $error) = $feed->searchTracks(0, $_GET['q']);
			$TMPL['messages'] = $tracks;
		}*/
		$arrData['tab'] = "top";
	}
	 elseif($_GET['filter'] == 'playlists') {
		$playlist = $feed->getPlaylists(0, 2, $_GET['q']);
		$error = $feed->showError('no_results', 1);
		$TMPL['messages'] = ((empty($playlist)) ? $error[0] : $playlist);
	}/* elseif($_GET['filter'] == 'artists') {
		list($tracks, $error) = $feed->searchArtists(0, $_GET['q']);
		$TMPL['messages'] = $tracks;
	}*/ elseif($_GET['filter'] == 'everything') {
		list($tracks, $error) = $feed->searchTracks(0, $_GET['q']);
		/*list($tracks2, $error) = $feed->searchArtists(0, $_GET['q']);*/
		//$TMPL['messages'] = $feed->getSearch(0, $settings['sperpage'], $_GET['q'], $_GET['filter']);
		$TMPL['messages'] = $tracks;
	} else {
		$TMPL['messages'] = $feed->getSearch(0, $settings['sperpage'], $_GET['q'], $_GET['filter']);
	}
	$feed->mysql_insert_array("search_log",$arrData);
	$rows = $skin->make();
	
	$skin = new skin('search/sidebar'); $sidebar = '';
	$TMPL['trending'] = $feed->sidebarTrending($_GET['tag'], 10);
	$TMPL['everything'] = $feed->sidebarEverything($_GET['filter'], $_GET['q']);
	$TMPL['filters'] = $feed->sidebarFilters($_GET['filter']);
	$TMPL['ad'] = generateAd($settings['ad6']);
	
	$sidebar = $skin->make();
	
	$TMPL = $TMPL_old; unset($TMPL_old);
	$TMPL['top'] = $top;
	$TMPL['rows'] = $rows;
	$TMPL['sidebar'] = $sidebar;

	$TMPL['url'] = $CONF['url'];
	$TMPL['title'] = $LNG['search'].' - '.htmlspecialchars($_GET['q']).' - '.$settings['title'];
	/*$TMPL['header'] = pageHeader($LNG['search'].' - '.$_GET['q']);*/
	
	$TMPL['tab1'] = ($_GET['filter'] == 'everything') ? 'active':'';
	$TMPL['tab2'] = ($_GET['filter'] == 'tracks') ? 'active':'';
	$TMPL['tab3'] = ($_GET['filter'] == 'playlists') ? 'active':'';
	$TMPL['tab4'] = ($_GET['filter'] == 'people') ? 'active':'';
	$TMPL['q'] = $_GET['q'];
	$skin = new skin('search/content');
	return $skin->make();
}
?>