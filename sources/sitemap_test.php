<?php 
session_start();
require_once('./includes/config.php');
require_once('./includes/classes.php');
define ("FREQUENCY", "always");
/*define ("OUTPUT_FILE", "sitemap.xml");

define ("IGNORE_EMPTY_CONTENT_TYPE", false);*/
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$resultSettings = $db->query(getSettings());
// Added to verify whether the user imported the database or not
if($resultSettings) {
    $settings = $resultSettings->fetch_assoc();
} else {
    echo "Error: ".$db->error;
}

header("Content-Type: application/xml; charset=utf-8"); 
echo '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL; 
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' .PHP_EOL; 
    $resultSongs = $db->query("select * from tracks");
    // Added to verify whether the user imported the database or not
    if($resultSongs) {
        while($allsongs = $resultSongs->fetch_assoc())
        {
            $resultArtist = $db->query("select first_name,last_name from users where idu='".$allsongs['uid']."' and type='artist'"); 
            if($resultArtist)
            {
                $artist_detail = $resultArtist->fetch_assoc();
                $artistName=$artist_detail['first_name']." ".$artist_detail['last_name'];
            } 
            else
            {
                $artistName="N/A";
            } 
            $trackUrl=$CONF['url']."/index.php?a=track&amp;id=".$allsongs['id']."&amp;name=".htmlspecialchars($allsongs['title']);
            $lastupdated=date("Y-m-d h:i A",strtotime($allsongs['time']));
            echo "<track>".PHP_EOL;
            echo "<artist>".htmlspecialchars($artistName)."</artist>".PHP_EOL;
            echo "<title>".htmlspecialchars($allsongs['title'])."</title>".PHP_EOL;
            echo "<genre>".$allsongs['tag']."</genre>".PHP_EOL;
            echo "<loc>".$trackUrl."</loc>".PHP_EOL;
            echo "<time>".$lastupdated."</time>".PHP_EOL;
            echo "<changefreq>".FREQUENCY."</changefreq>".PHP_EOL;
            echo "</track>".PHP_EOL;
        }
    } 
    echo '</urlset>';
    
?>